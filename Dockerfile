FROM node:10-alpine as build-step
RUN mkdir -p /gestion-crm
WORKDIR /gestion-crm
COPY package.json /gestion-crm
RUN npm install
COPY . /gestion-crm
RUN npm run build --prod
# Deployment Section
# Docker Image Section
FROM nginx:1.17.1-alpine
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=build-step /gestion-crm/dist/gestion-crm /usr/share/nginx/html