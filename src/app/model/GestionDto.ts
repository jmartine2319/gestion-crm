export class GestionDto{
    public tipoDocumento:string='';
    public numeroDocumento:number=0;
    public estado:string='';
    public comentario:string='';
}