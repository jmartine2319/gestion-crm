export class ClienteDto{
    public tipoDocumento:string='';
    public numeroDocumento:number=0;
    public nombre:string='';
    public apellido:string='';
    public correo:string='';
    public celular:number=0;
}