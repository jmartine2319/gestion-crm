import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ClienteDto } from '../model/ClienteDto';
import { GestionDto } from '../model/GestionDto';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private apiUrl: string = '';
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient:HttpClient) { 
    this.apiUrl = window.location.protocol + "//"+'127.0.0.1:8080/api';
  }

  crearCliente(cliente:ClienteDto ){

    return this.httpClient.post<any>(
      this.apiUrl + "/guardarCliente",
      cliente, this.httpOptions
    );

  }

  consultarCliente(cliente:ClienteDto ){
    return this.httpClient.post<any>(
      this.apiUrl + "/consultarCliente",
      cliente, this.httpOptions
    );
  }

  consultarClientes(){
    return this.httpClient.get<any>(
      this.apiUrl + "/consultarClientes",
    );
  }

  consultarTipoDoumento(){
    return this.httpClient.get<any>(
      this.apiUrl + "/consultarTipoDoc",
    );
  }

  crearGestion(gestion:GestionDto){
    return this.httpClient.post<any>(
      this.apiUrl + "/crearGestion",
      gestion, this.httpOptions
    );
  }

  consultarGestiones(cliente:ClienteDto){
    return this.httpClient.post<any>(
      this.apiUrl + "/obtenerGestiones",
      cliente, this.httpOptions
    );
  }

}
