import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { NgForm } from '@angular/forms';
import { ClienteDto } from 'src/app/model/ClienteDto';
import { GestionDto } from 'src/app/model/GestionDto';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.scss']
})
export class ResultadoComponent implements OnInit {

  public listaTipoDoc:string[]=[];
  public listaGestion:GestionDto[]=[];
  public mostrarResultado:boolean=false;
  public mostrarGestion:boolean=false;
  public clienteResultado:ClienteDto=new ClienteDto();

  constructor(public clienteService: ClienteService) { }

  ngOnInit(): void {
    this.consultarTipoDocumento();
  }

  consultarCliente(formulario:NgForm){
    console.log("consulta")
    let cliente:any = {
      tipoDocumento: formulario.value.tipodocumento,
      numeroDocumento: formulario.value.documento
    }
    this.clienteService.consultarCliente(cliente).subscribe(
      (data) => {
        console.log(data);
        this.mostrarResultado=true;
        this.clienteResultado=data;
      },
      (error) => {
        console.error(error)
      })
    this.clienteService.consultarGestiones(cliente).subscribe(
      (data) => {
        console.log(data);
        this.mostrarGestion=true;
        this.listaGestion=data;
      },
      (error) => {
        console.error(error)
      })
  }

  consultarTipoDocumento(){
    this.clienteService.consultarTipoDoumento().subscribe(
      (data) => {
        console.log(data);
        this.listaTipoDoc=data;
      },
      (error) => {
        console.error(error)
      })
  }

}
