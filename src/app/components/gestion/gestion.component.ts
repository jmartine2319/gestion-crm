import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { NgForm } from '@angular/forms';
import { GestionDto } from 'src/app/model/GestionDto';
import { ClienteDto } from 'src/app/model/ClienteDto';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.scss']
})
export class GestionComponent implements OnInit {
  public mostrarMensaje:boolean=false;
  public mostrarError:boolean=false;
  public mensaje:string='';
  public mostrarResultado:boolean=false;
  public clienteResultado:ClienteDto=new ClienteDto();
  public listaTipoDoc:string[]=[];

  constructor(public clienteService: ClienteService) { }

  ngOnInit(): void {
    this.consultarTipoDocumento();
  }

  consultarTipoDocumento(){
    this.clienteService.consultarTipoDoumento().subscribe(
      (data) => {
        console.log(data);
        this.listaTipoDoc=data;
      },
      (error) => {
        console.error(error)
      })
  }

  consultarCliente(formulario:NgForm){
    console.log("consulta")
    let cliente:any = {
      tipoDocumento: formulario.value.tipodocumento,
      numeroDocumento: formulario.value.documento
    }
    this.clienteService.consultarCliente(cliente).subscribe(
      (data) => {
        console.log(data);
        this.mostrarResultado=true;
        this.clienteResultado=data;
      },
      (error) => {
        console.error(error)
      })
  }

  crearGestion(formulario:NgForm){

    let gestion:GestionDto = {
      tipoDocumento: this.clienteResultado.tipoDocumento,
      numeroDocumento: this.clienteResultado.numeroDocumento,
      estado: formulario.value.estado,
      comentario: formulario.value.comentario
      
    }
    console.log(gestion);
    this.clienteService.crearGestion(gestion).subscribe(
      (data) => {
        console.log(data);
        this.mostrarMensaje=true;
        this.mostrarError=false;
        this.mensaje=data.mensaje;
      },
      (error) => {
        this.mostrarMensaje=false;
        this.mostrarError=true;
        console.error(error)
        this.mensaje=error.mensaje;
      })
  }

}
