import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ClienteDto } from 'src/app/model/ClienteDto';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
  public mostrarMensaje: boolean = false;
  public mostrarError: boolean = false;
  public mensaje: string = '';
  public listaClientes:ClienteDto[]=[];
  constructor(public clienteService: ClienteService) { }

  ngOnInit(): void {
    this.consultarClientes();
  }

  guardarCliente(formulario:NgForm){
    let parametro:ClienteDto = {
      tipoDocumento: formulario.value.tipodocumento,
      numeroDocumento: formulario.value.documento,
      nombre: formulario.value.nombre,
      apellido: formulario.value.apellido,
      correo: formulario.value.correo,
      celular: formulario.value.celular,
    }
    this.clienteService.crearCliente(parametro).subscribe(
      (data) => {
        console.log(data);
        this.mensaje=data.mensaje;
        this.mostrarMensaje=true;
        this.mostrarError=false;
        this.consultarClientes();
      },
      (error) => {
        console.error(error)
        this.mostrarError=true;
        this.mostrarMensaje=false;
        this.mensaje='Ocurrio un error inesperado'
        //this.utilidadesService.SetMessage(true, Constantes.MSJ_ERROR, error.error.mensaje);
      })
  }

  consultarClientes(){
    this.clienteService.consultarClientes().subscribe(
      (data) => {
        console.log(data)
        this.listaClientes = data;
      },
      (error) => {
        this.mensaje='Ocurrio un error inesperado'
          console.error(error)
      }
    );
  }
}
